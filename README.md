# README #

### What is this repository for? ###

This script was written to create virtual hosts using xampp in Windows. 
It will add an entry in to the /etc/hosts file and the xampp vhost.conf file. 

### How do I get set up? ###

1. Place file in your Sites or htdocs folder

2. Open file using your preferred text editor
3. check file paths. 
     i:  Check your /etc/hosts file is in the same location as mine
     ii: Check the file path for xampp's http-vhosts.conf
change paths according to your own

2. Open power shell 
      Start > Accessories > windows powershell 

 3. (Optional) 
       If this is your first time using Powershell enter:
       Set-ExecutionPolicy Unrestricted

       This will allow powershell to execute scripts as it is restricted by default

4. In Powershell navigate to the folder where you stored this script using the cd command

5. Execute file  using 
      .\vhost.ps1

When entering the folder destination for your site, you can drag and drop the folder into Powershell.
This will add the full path to that file for you. 

6. Open Site in browser
    To open site you must enter the url site.localhost.com:port
     eg if I named my site test and set xampp to use port 3000
     then to open the site i would enter 
     test.localhost.com:3000
