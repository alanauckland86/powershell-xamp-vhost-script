#####################################
#  Create Xampp virtual host        #
#  Script written by Alan Auckland  #
#  version 1  last edit 08/01/2015  #
#####################################

# 1. Allow powershell to execute scripts
	#Open Powershell and enter:  Set-ExecutionPolicy Unrestricted
	# This will allow powershell to always execute scripts

# 2 .Ask user for new site name
	 $site = Read-Host "New local site name: " 

# 3. Ask user for location of sites root folder
#(locatoin can be dragged and dropped into Powershell)
	$sitepath = Read-Host "Path to site ="

# 4. Write to host file site with .localhost.com appeneded
Add-Content C:\Windows\System32\Drivers\etc\hosts "`n127.0.0.1`t $site.localhost.com "

# 5. Write to xampp vhosts config file 
Add-Content E:\applications\xampp\apache\conf\extra\httpd-vhosts.conf `
"<VirtualHost *> `
 	ServerName $site.localhost.com `
 	DocumentRoot $sitepath	`
 	<Directory $sitepath> `
 		Options Indexes FollowSymLinks MultiViews `
 		AllowOverride All `
 		Order allow,deny `
 		allow from all `
 	</Directory> `
 </VirtualHost> "

# 6. restart xampp (Start apperas 2x becuase apache would error if it wasn't started)
E:\applications\xampp\xampp_start.exe
E:\applications\xampp\xampp_stop.exe
E:\applications\xampp\xampp_start.exe


#Navigate site url in browser and add :{portnumber}
#eg  testsite.localhost.com:8080